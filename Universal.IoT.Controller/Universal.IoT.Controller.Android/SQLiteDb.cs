﻿using System;
using System.IO;
using SQLite;
using Universal.IoT.Controller.Droid;
using Universal.IoT.Controller.Persistence;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteDb))]

namespace Universal.IoT.Controller.Droid
{
	public class SQLiteDb : ISQLiteDb
	{
		public SQLiteConnection GetConnection()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var path = Path.Combine(documentsPath, "Universal.IoT.db3");

			return new SQLiteConnection(path);
		}
	}
}

