﻿namespace Universal.IoT.Controller.Models
{
    public class BoolPin : Pin
    {
        public BoolPin(Pin pin) : base(pin)
        {
        }
        private bool boolVal;
        public bool BoolVal
        {
            get { return boolVal; }
            set
            {
                boolVal = value;
                base.Value = value.ToString();
            }
        }
    }
}
