﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace Universal.IoT.Controller.Models
{
    public class Device
    {
        public Device()
        {
            Pins = new List<Pin>();
        }
        [PrimaryKey,AutoIncrement]
        public byte Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }
        [OneToMany]
        public List<Pin> Pins { get; set; }
    }
}
