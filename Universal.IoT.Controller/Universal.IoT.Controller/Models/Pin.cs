﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using Universal.IoT.Controller.Enums;

namespace Universal.IoT.Controller.Models
{
    public class Pin
    {
        public Pin()
        {
        }
        public Pin(Pin pin)
        {
            Id = pin.Id;
            Name = pin.Name;
            ParameterDirection = pin.ParameterDirection;
            PinDataType = pin.PinDataType;
            Value = pin.Value;
        }
        [PrimaryKey, AutoIncrement]
        public byte Id { get; set; }

        [ForeignKey(typeof(Device))]
        public int DeviceId { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        public ParameterDirection ParameterDirection { get; set; }
        public PinDataType PinDataType { get; set; }
        [Ignore]
        public string ProposedValue { get; set; }
        public DateTime ProposedValueTimestamp { get; set; }
        public virtual string Value { get; set; }
        public DateTime ValueTimestamp { get; set; }
    }
}
