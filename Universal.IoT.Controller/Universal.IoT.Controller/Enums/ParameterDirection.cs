﻿namespace Universal.IoT.Controller.Enums
{
    public enum ParameterDirection
    {
        InputOutput,
        Input,
        Output
    }
}
