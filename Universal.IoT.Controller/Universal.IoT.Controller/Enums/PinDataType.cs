﻿namespace Universal.IoT.Controller.Enums
{
    public enum PinDataType
    {
        StringType,
        IntType,
        BoolType
    }
}
