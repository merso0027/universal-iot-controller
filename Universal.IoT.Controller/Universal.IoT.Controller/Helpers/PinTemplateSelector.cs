﻿using Universal.IoT.Controller.Models;
using Xamarin.Forms;

namespace Universal.IoT.Controller.Helpers
{
    public class PinTemplateSelector : DataTemplateSelector
    {
        public DataTemplate IntPinTemplate { get; set; }

        public DataTemplate StringPinTemplate { get; set; }

        public DataTemplate BoolPinTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is IntPin)
                return IntPinTemplate;

            if (item is StringPin)
                return StringPinTemplate;

            if (item is BoolPin)
                return BoolPinTemplate;

            return null;
        }
    }
}
