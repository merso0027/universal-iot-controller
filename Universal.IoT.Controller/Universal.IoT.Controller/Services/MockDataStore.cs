﻿using System.Collections.Generic;
using System.Linq;
using Universal.IoT.Controller.Models;

namespace Universal.IoT.Controller.Services
{
    public class MockDataStore : IDataStore<Device>
    {
        readonly List<Device> devices;

        public MockDataStore()
        {
            devices = new List<Device>();
        }

        public void AddItem(Device item)
        {
            var maxId = 1;
            if (devices.Count() != 0)
                maxId = devices.Max(f => f.Id) + 1;
            item.Id = (byte)(maxId);

            var maxPinId = 1;
            if (devices.Count() != 0)
                maxPinId = devices.SelectMany(b => b.Pins).Max(f => f.Id);
            foreach (var pin in item.Pins)
            {
                maxPinId++;
                pin.Id = (byte)(maxPinId);
            }
            devices.Add(item);
        }

        public void DeleteItem(Device item)
        {
            var dev = devices.FirstOrDefault(i => i.Id == item.Id);
            devices.Remove(dev);
        }

        public Device GetItem(byte id)
        {
            var device = devices.FirstOrDefault(f => f.Id == id);
            return device;
        }

        public IEnumerable<Device> GetItems(bool forceRefresh = false)
        {
            return devices;
        }

        public Pin GetPin(byte id)
        {
            throw new System.NotImplementedException();
        }

        public Pin UpdatePin(Pin item)
        {
            throw new System.NotImplementedException();
        }

        public void UpdatePinValues(byte deviceId, IEnumerable<Pin> enumerablePins)
        {
            var device = devices.FirstOrDefault(f => f.Id == deviceId);
            foreach (var item in device.Pins)
            {
                if (enumerablePins.Any(f => f.Id == item.Id))
                    item.Value = enumerablePins.First(f => f.Id == item.Id).Value;
            }
        }
    }
}