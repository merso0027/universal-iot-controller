﻿using System.Collections.Generic;
using Universal.IoT.Controller.Models;
using Xamarin.Forms;

namespace Universal.IoT.Controller.Services
{
    public class MockTestData
    {
        public IDataStore<Models.Device> DataStore => DependencyService.Get<IDataStore<Models.Device>>();
        public void SetTestData()
        {
            var device1 = new Models.Device()
            {
                Name = "Test device 1",
                Description = "This is first device for testing",
                Pins = new List<Pin>()
                {
                       new Pin(){
                           Name="Name",
                           ParameterDirection=Enums.ParameterDirection.InputOutput,
                           PinDataType=Enums.PinDataType.StringType
                       },
                       new Pin(){
                           Name="Enabled",
                           ParameterDirection=Enums.ParameterDirection.InputOutput,
                           PinDataType=Enums.PinDataType.BoolType
                       },
                       new Pin(){
                           Name="Number",
                           ParameterDirection=Enums.ParameterDirection.InputOutput,
                           PinDataType=Enums.PinDataType.IntType
                       },
                }
            };

            var device2 = new Models.Device()
            {
                Name = "Test device 2",
                Description = "This is second device for testing",
                Pins = new List<Pin>()
                {
                       new Pin(){
                           Name="First name",
                           ParameterDirection=Enums.ParameterDirection.InputOutput,
                           PinDataType=Enums.PinDataType.StringType
                       },
                       new Pin(){
                           Name="Middle name",
                           ParameterDirection=Enums.ParameterDirection.InputOutput,
                           PinDataType=Enums.PinDataType.StringType
                       },
                       new Pin(){
                           Name="Last name",
                           ParameterDirection=Enums.ParameterDirection.InputOutput,
                           PinDataType=Enums.PinDataType.StringType
                       },
                }
            };
            DataStore.AddItem(device1);
            DataStore.AddItem(device2);
        }
    }
}
