﻿using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Universal.IoT.Controller.Models;
using Universal.IoT.Controller.Persistence;
using Xamarin.Forms;

namespace Universal.IoT.Controller.Services
{
    public class SQLiteDbDataStore : IDataStore<Models.Device>
    {
        protected SQLiteConnection connection;
        public SQLiteDbDataStore()
        {
            connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            connection.CreateTable<Models.Device>();
            connection.CreateTable<Models.Pin>();
        }
        public void AddItem(Models.Device item)
        {
            connection.Insert(item);
            foreach (var pin in item.Pins)
            {
                connection.Insert(pin);
            }
            connection.UpdateWithChildren(item);
        }

        public void DeleteItem(Models.Device item)
        {
            connection.Delete(item);
        }

        public Models.Device GetItem(byte id)
        {
            return connection.GetAllWithChildren<Models.Device>().First(f => f.Id == id);
        }

        public IEnumerable<Models.Device> GetItems(bool forceRefresh = false)
        {
            return connection.GetAllWithChildren<Models.Device>();
        }

        public void UpdatePinValues(byte deviceId, IEnumerable<Pin> enumerablePins)
        {
            var device = connection.GetAllWithChildren<Models.Device>().First(f => f.Id == deviceId);
            var updatePins = new List<Pin>();
            foreach (var item in device.Pins)
            {
                if (enumerablePins.Any(f => f.Id == item.Id))
                {
                    var dbItem = GetPin(item.Id);
                    dbItem.Value = enumerablePins.First(f => f.Id == item.Id).Value;
                    updatePins.Add(dbItem);
                }
            }
            connection.UpdateAll(updatePins);
        }
        public Pin GetPin(byte id)
        {
            var res = connection.GetAllWithChildren<Models.Pin>().First(f => f.Id == id);
            res.ValueTimestamp = DateTime.Now;
            connection.Update(res);
            return res;
        }

        public Pin UpdatePin(Pin item)
        {
            var pin = connection.Get<Models.Pin>(f => f.Id == item.Id);
            pin.Value = item.ProposedValue;
            pin.ProposedValueTimestamp = DateTime.Now;
            connection.Update(pin);
            return pin;
        }
    }
}
