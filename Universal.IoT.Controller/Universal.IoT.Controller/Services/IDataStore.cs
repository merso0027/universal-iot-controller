﻿using System.Collections.Generic;
using Universal.IoT.Controller.Models;

namespace Universal.IoT.Controller.Services
{
    public interface IDataStore<T>
    {
        void AddItem(T item);
        T GetItem(byte id);
        void DeleteItem(T item);
        IEnumerable<T> GetItems(bool forceRefresh = false);
        void UpdatePinValues(byte deviceId, IEnumerable<Pin> enumerablePins);
        Pin UpdatePin(Pin item);
        Pin GetPin(byte id);
    }
}
