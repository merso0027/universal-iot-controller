﻿using System;
using System.Windows.Input;
using Universal.IoT.Controller.Views;
using Xamarin.Forms;

namespace Universal.IoT.Controller.ViewModels
{
    public class CreateDeviceViewModel : BaseViewModel
    {
        private INavigation navigation;
        public ICommand NavigateToDetailsCommand { get; private set; }
        public ICommand CancelCommand { get; private set; }
        public Models.Device Device { get; set; }
        public CreateDeviceViewModel(INavigation navigation)
        {
            this.navigation = navigation;
            Device = new Models.Device();
            NavigateToDetailsCommand = new Command(NavigateToDetails);
            CancelCommand = new Command(async () => await navigation.PopModalAsync());
        }

        private void NavigateToDetails()
        {
            if (String.IsNullOrWhiteSpace(Device.Name))
            {
                Application.Current.MainPage.DisplayAlert("Alert", "Device name is mandatory.", "ok");
                return;
            }
            navigation.PushAsync(new NewPinsPage(Device));
        }
    }
}
