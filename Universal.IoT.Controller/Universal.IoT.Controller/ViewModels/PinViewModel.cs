﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Universal.IoT.Controller.Enums;
using Universal.IoT.Controller.Models;
using Xamarin.Forms;

namespace Universal.IoT.Controller.ViewModels
{
    public class PinViewModel : BaseViewModel
    {
        private Pin pin;
        public Pin Pin
        {
            get { return pin; }
            set { SetProperty(ref pin, value); }
        }

        private string proposedValueText;
        public string ProposedValueText
        {
            get { return proposedValueText; }
            set { SetProperty(ref proposedValueText, value); }
        }

        private string valueText;
        public string ValueText
        {
            get { return valueText; }
            set { SetProperty(ref valueText, value); }
        }

        private string proposedBoolValue;
        public string ProposedBoolValue
        {
            get { return proposedBoolValue; }
            set { SetProperty(ref proposedBoolValue, value); }
        }
        
        public ICommand CancelCommand { get; private set; }
        public ICommand RefreshPinCommand { get; private set; }
        public ICommand SendPinCommand { get; private set; }
        public PinViewModel(Pin pin, INavigation navigation)
        {
            Pin = pin;
            ProposedValueText=String.Empty;
            ValueText = String.Empty;
            ValueTimestampText = string.Empty;
            ProposedValueTimestampText = string.Empty;
            UniqueValue = Guid.NewGuid().ToString();
            CancelCommand = new Command(async () => await navigation.PopModalAsync());
            RefreshPinCommand = new Command(() => RefreshPin());
            SendPinCommand = new Command(() => SendPin());
            IsVisibleEntry = pin.PinDataType != PinDataType.BoolType;
            IsVisibleToggle = !IsVisibleEntry;
            keyboardType = pin.PinDataType == PinDataType.IntType ? Keyboard.Numeric : Keyboard.Default;
        }

        private void SendPin()
        {
            this.Pin.ProposedValue = proposedValueText;
            if (this.Pin.PinDataType == PinDataType.BoolType)
                this.Pin.ProposedValue = proposedBoolValue.ToString();
            var currentPin = DataStore.UpdatePin(Pin);
            ProposedValueText = currentPin.ProposedValue;
            ProposedValueTimestampText = currentPin.ProposedValueTimestamp.ToString();
        }

        private void RefreshPin()
        {
            var currentPin = DataStore.GetPin(Pin.Id);
            ValueText = currentPin.Value;
            ValueTimestampText = currentPin.ValueTimestamp.ToString();
        }

        public ICommand RemovePinCommand
        {
            get
            {
                return new Command(() =>
                {
                    MessagingCenter.Send<PinViewModel, string>(this, "RemovePin", UniqueValue);
                });
            }
        }

        public string UniqueValue { get; private set; }

        private string selectedParameterDirection;
        public string SelectedParameterDirection
        {
            set
            {
                ParameterDirection parameterDirection;
                Enum.TryParse(value, out parameterDirection);
                Pin.ParameterDirection = parameterDirection;
                selectedParameterDirection = value;
            }

            get
            {
                if (string.IsNullOrWhiteSpace(selectedParameterDirection))
                    selectedParameterDirection = ParameterDirection.InputOutput.ToString();
                return selectedParameterDirection;
            }
        }

        public List<string> ParameterDirectionNames
        {
            get
            {
                return System.Enum.GetNames(typeof(ParameterDirection)).ToList();
            }
        }

        private string valueTimestampText;
        public string ValueTimestampText
        {
            get { return valueTimestampText; }
            private set { SetProperty(ref valueTimestampText, value); }
        }

        private string proposedValueTimestampText;
        public string ProposedValueTimestampText
        {
            get { return proposedValueTimestampText; }
            private set { SetProperty(ref proposedValueTimestampText, value); }
        }


        private string selectedDataType;
        public String SelectedDataType
        {
            set
            {
                PinDataType parameterDataType;
                Enum.TryParse(value, out parameterDataType);
                Pin.PinDataType = parameterDataType;
                selectedDataType = value;
            }
            get
            {
                if (string.IsNullOrWhiteSpace(selectedDataType))
                    selectedDataType = PinDataType.StringType.ToString();
                return selectedDataType;
            }
        }

        public List<string> ParameterDataTypes
        {
            get
            {
                return System.Enum.GetNames(typeof(PinDataType)).ToList();
            }
        }

        private bool isVisibleEntry;
        public bool IsVisibleEntry
        {
            get { return isVisibleEntry; }
            private set { SetProperty(ref isVisibleEntry, value); }
        }
        private bool isVisibleToggle;
        public bool IsVisibleToggle
        {
            get { return isVisibleToggle; }
            private set { SetProperty(ref isVisibleToggle, value); }
        }

        private Keyboard keyboardType;
        public Keyboard KeyboardType
        {
            get { return keyboardType; }
            private set { SetProperty(ref keyboardType, value); }
        }
    }
}
