﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;
using Universal.IoT.Controller.Views;
using System.Windows.Input;
using Universal.IoT.Controller.Services;
using System.Linq;
using Prism.Commands;

namespace Universal.IoT.Controller.ViewModels
{
    public class DevicesViewModel : BaseViewModel
    {
        private INavigation navigation;
        public ICommand AddItemCommand { get; private set; }

        public ICommand SetTestDeviceCommand { get; private set; }
        public DelegateCommand<Models.Device> DeleteCommand { get; set; }
        public ObservableCollection<Models.Device> Devices { get; set; }
        public DevicesViewModel(INavigation navigation)
        {
            this.navigation = navigation;
            Devices = new ObservableCollection<Models.Device>();
            ExecuteLoadDevices();
            WhenDataNotExist = Devices.Count() == 0;
            DeleteCommand = new DelegateCommand<Models.Device>(f => DeleteDevice(f));
            SetTestDeviceCommand = new Command(() => SetTestData());
            AddItemCommand = new Command(async () => await navigation.PushModalAsync(new NavigationPage(new NewDevicePage())));
        }

        private async void DeleteDevice(Models.Device device)
        {
            var isDeleted = await Application.Current.MainPage.DisplayAlert("Alert", "Are you sure you want to delete this device?", "Ok", "Cancel");
            if (isDeleted)
            {
                DataStore.DeleteItem(device);
                Devices.Remove(device);
                WhenDataNotExist = Devices.Count() == 0;
            }
        }

        private void SetTestData()
        {
            var mockTestData = new MockTestData();
            mockTestData.SetTestData();
            ExecuteLoadDevices();
            WhenDataNotExist = Devices.Count() == 0;
        }

        private Models.Device selectedDevice;
        public Models.Device SelectedDevice
        {
            get
            {
                return selectedDevice;
            }

            set
            {
                if (value != null)
                {
                    DeviceSelected(value);
                    selectedDevice = null;
                    OnPropertyChanged();
                }
            }
        }

        private bool whenDataNotExist;
        public bool WhenDataNotExist
        {
            get { return whenDataNotExist; }
            set { SetProperty(ref whenDataNotExist, value); }
        }

        private async void DeviceSelected(Models.Device device)
        {
            if (device == null)
                return;

            Application.Current.Properties["DeviceId"] = device.Id;
            await Application.Current.SavePropertiesAsync();
            await navigation.PushAsync(new DeviceTabbedPage());
        }

        private void ExecuteLoadDevices()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Devices.Clear();
                var items = DataStore.GetItems(true);
                foreach (var item in items)
                {
                    Devices.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}