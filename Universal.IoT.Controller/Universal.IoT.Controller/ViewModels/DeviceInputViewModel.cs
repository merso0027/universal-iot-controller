﻿using Prism.Commands;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Universal.IoT.Controller.Enums;
using Universal.IoT.Controller.Models;
using Universal.IoT.Controller.Views;
using Xamarin.Forms;

namespace Universal.IoT.Controller.ViewModels
{
    public class DeviceInputViewModel : BaseViewModel
    {
        private byte deviceId;
        private readonly INavigation navigation;
        public ICommand PostPinsCommand { get; set; }
        public ObservableCollection<Pin> Pins { get; set; } = new ObservableCollection<Pin>();
        public DelegateCommand<Pin> NavigateToPinDetailsCommand { get; set; }
        public DeviceInputViewModel(byte deviceId, INavigation navigation)
        {
            this.navigation = navigation;
            this.deviceId = deviceId;
            var allPins = DataStore.GetItem(deviceId).Pins.Where(f => f.ParameterDirection != ParameterDirection.Output);
            SetPins(allPins);
            PostPinsCommand = new Command(Post);
            NavigateToPinDetailsCommand = new DelegateCommand<Pin>(f => NavigateToPinDetails(f));
        }

        private async void NavigateToPinDetails(Pin pin)
        {
            await navigation.PushModalAsync(new NavigationPage(new PinDetailsPage(pin)));
        }

        private void Post()
        {
            var enumerablePins = Pins.AsEnumerable();
            DataStore.UpdatePinValues(deviceId, enumerablePins);
            MessagingCenter.Send<DeviceInputViewModel, byte>(this, "UpdatePins", deviceId);
            MessagingCenter.Send<DeviceInputViewModel, int>(this, "SetOutputTab", 1);
        }

        private void SetPins(IEnumerable<Pin> allPins)
        {
            foreach (var item in allPins)
            {
                switch (item.PinDataType)
                {
                    case PinDataType.IntType:
                        Pins.Add(new IntPin(item));
                        continue;
                    case PinDataType.StringType:
                        Pins.Add(new StringPin(item));
                        continue;
                    case PinDataType.BoolType:
                        Pins.Add(new BoolPin(item));
                        continue;
                    default:
                        continue;
                }
            }
        }
    }
}
