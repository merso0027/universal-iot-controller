﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Universal.IoT.Controller.Models;
using Universal.IoT.Controller.Views;
using Xamarin.Forms;

namespace Universal.IoT.Controller.ViewModels
{
    public class CreatePinsViewModel : BaseViewModel
    {
        private Models.Device device;
        private INavigation navigation;
        public ICommand FinishCommand { get; set; }
        public ICommand AddNewPinCommand { get; set; }
        public ObservableCollection<PinViewModel> PinViewModels { get; set; } = new ObservableCollection<PinViewModel>();
        public CreatePinsViewModel(Models.Device device, INavigation navigation)
        {
            this.device = device;
            this.navigation = navigation;
            FinishCommand = new Command(() => Finish());
            AddNewPinCommand = new Command(() => AddNewPin());

            MessagingCenter.Subscribe<PinViewModel, string>(this, "RemovePin", (sender, UniqueValue) =>
            {
                var pin = PinViewModels.FirstOrDefault(f => f.UniqueValue == UniqueValue);
                if (pin != null)
                    PinViewModels.Remove(pin);
            });
        }

        private void AddNewPin()
        {
            PinViewModels.Add(new PinViewModel(new Pin(), navigation));
        }
        private void Finish()
        {
            var pins = PinViewModels.Select(f => f.Pin).AsEnumerable();
            if (pins.Any(f => String.IsNullOrWhiteSpace(f.Name)))
            {
                Application.Current.MainPage.DisplayAlert("Alert", "Pin name is mandatory.", "ok");
                return;
            }
            if (pins.Count() == 0)
            {
                Application.Current.MainPage.DisplayAlert("Alert", "You must add at least one pin.", "ok");
                return;
            }
            device.Pins = pins.ToList();
            DataStore.AddItem(device);
            navigation.PushAsync(new MainPage());
        }
    }
}
