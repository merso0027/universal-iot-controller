﻿using System.Collections.ObjectModel;
using System.Linq;
using Universal.IoT.Controller.Enums;
using Universal.IoT.Controller.Models;
using Xamarin.Forms;

namespace Universal.IoT.Controller.ViewModels
{
    public class DeviceOutputViewModel : BaseViewModel
    {
        public ObservableCollection<Pin> Pins { get; private set; } = new ObservableCollection<Pin>();
        public DeviceOutputViewModel(byte deviceId)
        {
            SetPins(deviceId);
            MessagingCenter.Subscribe<DeviceInputViewModel, byte>(this, "UpdatePins", (d, id) => {
                SetPins(id);
            });
        }

        private void SetPins(byte deviceId) {
            var allPins = DataStore.GetItem(deviceId).Pins.Where(f => f.ParameterDirection != ParameterDirection.Input);
            Pins.Clear();
            foreach (var item in allPins)
            {
                Pins.Add(new Pin(item));
            }
        }
    }
}
