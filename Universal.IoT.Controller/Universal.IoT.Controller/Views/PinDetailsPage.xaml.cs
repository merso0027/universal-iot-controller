﻿using Universal.IoT.Controller.Models;
using Universal.IoT.Controller.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Universal.IoT.Controller.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PinDetailsPage : ContentPage
    {
        public PinDetailsPage(Pin pin)
        {
            InitializeComponent();
            BindingContext = new PinViewModel(pin,Navigation);
        }
    }
}