﻿using Universal.IoT.Controller.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Universal.IoT.Controller.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeviceInputPage : ContentPage
    {
        public DeviceInputPage()
        {
            InitializeComponent();
            var deviceId = (byte)Application.Current.Properties["DeviceId"];
            BindingContext = new DeviceInputViewModel(deviceId,Navigation);
        }
    }
}