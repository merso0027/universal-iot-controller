﻿using System.ComponentModel;
using Universal.IoT.Controller.ViewModels;
using Xamarin.Forms;

namespace Universal.IoT.Controller.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class DeviceTabbedPage : TabbedPage
    {
        public DeviceTabbedPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            MessagingCenter.Subscribe<DeviceInputViewModel, int>(this, "SetOutputTab", (sender, index) => {
                CurrentPage = Children[index];
            });
        }

    }
}