﻿using Universal.IoT.Controller.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Universal.IoT.Controller.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeviceOutputPage : ContentPage
    {
        public DeviceOutputPage()
        {
            InitializeComponent();
            var deviceId = (byte)Application.Current.Properties["DeviceId"];
            BindingContext = new DeviceOutputViewModel(deviceId);
        }
    }
}