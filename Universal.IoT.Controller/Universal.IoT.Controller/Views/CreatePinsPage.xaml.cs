﻿
using Universal.IoT.Controller.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Universal.IoT.Controller.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewPinsPage : ContentPage
    {
        public NewPinsPage(Models.Device device)
        {
            InitializeComponent();
            BindingContext = new CreatePinsViewModel(device, Navigation);
        }
    }
}