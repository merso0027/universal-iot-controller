﻿using SQLite;

namespace Universal.IoT.Controller.Persistence
{
    public interface ISQLiteDb
    {
        SQLiteConnection GetConnection();
    }
}
